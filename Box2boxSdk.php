<?php

namespace box2box\box2box;

use Exception;
use Guzzle\Http\Client;

class Box2boxSdk
{
    /**
     * @var - Версия API
     */
    static private $version;

    /**
     * @var - url API
     */
    static private $baseUrl;

    static private $login;

    static private $password;

    static private $decodeJson;

    /**
     * @var - Токен используется для подписи запросов к Api
     */
    static private $token = "90e7e78a69ce67f0c4e6400283e7832f";

    /**
     * @param $url - урл, на который будем стучать
     * Например, http://api.box2box.ru
     * @param $login
     * @param $password
     * @param $version - версия API
     * @param bool $decode_json - конвертировать ли json-строку ответа
     * @throws Exception
     */
    static public function init($base_url, $login, $password, $version = 1, $decode_json = true)
    {
        if (
            is_string($base_url) && !empty($base_url)
            && is_string($login) && !empty($login)
            && is_string($password) && !empty($password)
            && !empty($version)
        ) {
            self::$login = trim($login);
            self::$password = trim($password);
            self::$baseUrl = trim($base_url, " \t\n\r\0\x0B/");
            self::$version = trim($version);
        } else {
            throw new Exception('Base_url, version, login and password might be a non empty string!');
        }

        self::$decodeJson = (bool)$decode_json;
    }

    static private function getToken()
    {
        if (empty(self::$token)) {
            $url = self::$baseUrl . '/v' . self::$version . '/users/' . self::$login . '/' . self::$password . '/token';
            $response = self::call('GET', $url, null);
            self::$token = $response->result->key;
        }

        return self::$token;
    }

    /**
     * Создание или редактирование заказ (в зависимости от наличия идентификатора заказа)
     * @param array $numbers - Список номеров заказа
     * @param string $pickuppoint - ID ПВЗ
     * @param array $costs - Список стоимостей заказа в формате (оценочная и реальная)
     * @param array $dimensions - Массо-габаритные характеристики заказа (длина, ширина, высота, вес)
     * @param array $recipient - Данные о получателе заказа
     * @param array $items - Список вложимого
     * @param string $id - Идентификатор заказа (для редактирования)
     */
    static public function orders(array $numbers, $pickuppoint, array $costs, array $dimensions, array $recipient, array $items, $id = null)
    {
        $token = self::getToken();
        $url = $url = self::$baseUrl . '/v' . self::$version . '/orders' . ($id ? '/' . $id : '');
        $body = json_encode([
            'numbers' => $numbers,
            'pickuppoint' => $pickuppoint,
            'costs' => $costs,
            'dimensions' => $dimensions,
            'recipient' => $recipient,
            'items' => $items,
        ]);

        return self::call('POST', $url, $token, self::$decodeJson, $body);
    }

    /**
     * Возвращает информацию о публичном ПВЗ по идентификатору.
     * ИЛИ
     * Возвращает публичный список ПВЗ.
     *
     * если передан id точки, то можно указать только fields
     *
     * @param string /null $id - Идентификатор ПВЗ
     * @param string /null $fields - Список полей для вывода (разделённых запятой)
     * @param string /null $filter - Список фильтров (разделённых запятой, например "name=The name,type=the_type"). Используйте '!' для поиска строгого соответствия (name=!The name)
     * @param string /null $orderBy - Сортировать по заданному полю. Используйте префикс '-' для сортировки в обратном порядке (-name)
     * @param int /null $limit - Лимит выборки
     * @param int /null $offset - Пропустить заданное количество результатов
     */
    static public function pickupPointsPublic($id = null, $fields = null, $filter = null, $orderBy = null, $limit = null, $offset = null)
    {
        $token = self::getToken();
        $url = $url = self::$baseUrl . '/v' . self::$version . '/pickuppoints/' . ($id ? $id . '/' : '') . 'public';
        $query = [];
        if (!empty($fields)) $query['fields'] = (string)$fields;
        //если передан id точки, то можно указать только fields
        if (!$id) {
            if (!empty($filter)) $query['filter'] = (string)$filter;
            if (!empty($orderBy)) $query['orderBy'] = (string)$orderBy;
            if (!empty($limit)) $query['limit'] = (int)$limit;
            if (!empty($offset)) $query['offset'] = (int)$offset;
        }

        return self::call('GET', $url, $token, self::$decodeJson, null, $query);
    }

    /**
     * Возвращает историю изменения статусов заказа
     * @param string $id - Идентификатор заказа
     */
    static public function ordersStatusesHistory($id)
    {
        $token = self::getToken();
        $url = $url = self::$baseUrl . '/v' . self::$version . '/orders/' . $id . '/statuses/history';

        return self::call('GET', $url, $token, self::$decodeJson);
    }

    /**
     * Создает заявку на забор
     * @param array $data - Данные заявки
     * @return object
     * @throws Exception
     */
    static public function requests(array $data)
    {
        if(empty($data)){
            throw new Exception('Data can not be empty!');
        }
        $token = self::getToken();
        $url = $url = self::$baseUrl . '/v' . self::$version . '/requests';
        $body = json_encode([
            'data' => $data
        ]);

        return self::call('POST', $url, $token, self::$decodeJson, $body);
    }


    /** Общий метод для отправки запроса
     * @param $method - метод (GET, POST, ....)
     * @param $url - на какой url отправлять запрос
     * @param $token - токен для подписи запросов
     * @param $decode_json - отдавать json-строку или конвертировать в объект
     * @param mixed $body - тело запроса (для метода POST)
     * @param array $query - параметры GET запроса
     * @return object
     * @throws Exception
     */
    static private function call($method, $url, $token, $decode_json=true, $body=null, array $query=[])
    {
        try{
            $client = new Client();
            $request = $client->createRequest($method, $url, null, $body, ['query'=>$query]);
            $request->setHeader('Authorization', $token);
            $request->setHeader('Content-Type', 'application/json');
            if($body){
                $request->setHeader('Content-Length', strlen($body));
            }
            $response = $request->send();
            if($response->isSuccessful()){
                if(!$response->getBody()->getContentLength()){
                    return (object)['result'=> null,  'url'=>$request->getUrl()];
                }

                $response_body = $response->getBody(true);

                if($decode_json){
                    $result = (object)['result'=>json_decode($response_body), 'url'=>$request->getUrl()];
                } else {
                    $result = (object)['result'=>$response_body, 'url'=>$request->getUrl()];
                }

                return $result;
            }

            throw new Exception($response->getMessage());
        }
        catch(Exception $e){
            //нормальное описание ошибки приходи в теле ответа
            $error_respoce = $e->getResponse();
            $error_body = json_decode($error_respoce->getBody(true));
            //проверка на валидный json в теле ответа
            if(json_last_error() == JSON_ERROR_NONE){
                throw new Exception($error_body->error->message, $error_body->error->code, $e);
            } else {
                throw new Exception($e->getMessage(), $e->getCode(), $e);
            }
        }
    }

    static public function getStatusesList()
    {
        return [];
    }

}